TODO
====
*Add new applications*
        * authy-desktop         https://authy.com/

*Add missing cores*
        
        * bsnes-libretro-cplusplus98
        * chailove-libretro
        * daphne-libretro
        * dolphin-libretro
        * easyrpg-libretro
        * emux-chip8-libretro
        * emux-gb-libretro
        * emux-nes-libretro
        * emux-sms-libretro
        * ffmpeg-libretro
        * fbalpha2012-cps1-libretro
        * fbalpha2012-cps2-libretro
        * fbalpha2012-cps3-libretro
        * fbalpha2012-neogeo-libretro
        * fsuae-libretro
        * gearboy-libretro
        * gearsystem-libretro
        * higan-sfc-libretro
        * ishiiruka-libretro
        * kronos-libretro
        * mame2003plus-libretro
        * mesen-libretro
        * mess2015-libretro
        * mupen64plus-libretro
        * nekop2-libretro
        * np2kai-libretro
        * play-libretro
        * ppsspp-libretro
        * redbook-libretro
        * remotejoy-libretro
        * squirreljme-libretro
        * stonesoup-libretro
        * thepowdertoy-libretro
        * tic80-libretro
        * ume2015-libretro
        * uzem-libretro
        * vemulator-libretro
